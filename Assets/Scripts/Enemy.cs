using System;
using UnityEngine;

namespace TS
{
    public class Enemy : MonoBehaviour
    {
        [SerializeField] protected int Hp = 10;
        
        private void OnCollisionEnter2D(Collision2D other)
        {
                                
            var player = other.collider.GetComponent<Player>();
            if (player != null)
            {
                //发生碰撞
                Hp -= player.AttackAmount;
            }
        }
    }
}
