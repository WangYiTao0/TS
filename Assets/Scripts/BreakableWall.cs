using System;
using UnityEngine;

namespace TS
{
    public class BreakableWall : MonoBehaviour
    {
        [SerializeField] private int _canTakeHitAmount = 2;
        
        private int _hitCount = 0;

        private void Update()
        {
            if (_hitCount >= _canTakeHitAmount)
            {
                Destroy(gameObject);
            }
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            var player = other.collider.GetComponent<Player>();
            if (player != null)
            {
                _hitCount++;
            }
        }
    }
}