using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace TS
{
    public class Player : MonoBehaviour
    {
        [SerializeField] private float _ballPower = 2000f;
        [SerializeField] public int AttackAmount;
        [SerializeField] public int Hp;
        
        private Rigidbody2D _rigidBody2d;
        private SpriteRenderer _spriteRenderer;
        private bool _select = false;
        private Vector3 _startPos = Vector3.zero;
        private Color _originColor;
        
        private void Start()
        {
            _rigidBody2d = GetComponent<Rigidbody2D>();
            _spriteRenderer = GetComponent<SpriteRenderer>();
            _startPos = _rigidBody2d.position;
            _originColor = _spriteRenderer.color;
        }
        
        private void OnMouseDown()
        {            
            _startPos = _rigidBody2d.position;
            _spriteRenderer.color = Color.red;
        }

        private void OnMouseDrag()
        {
            var mousePosition =  Camera.main.ScreenToWorldPoint(Input.mousePosition);
            transform.position = new Vector3(mousePosition.x,mousePosition.y,transform.position.z);
        }

        private void OnMouseUp()
        {
            Vector3 currentPosition = _rigidBody2d.position;
            Vector3 direction = _startPos - currentPosition;
            direction.Normalize();
            _rigidBody2d.AddForce(direction * _ballPower,ForceMode2D.Impulse);
            _spriteRenderer.color = _originColor;
        }
    }
}